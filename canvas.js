const canvas = document.querySelector('canvas');
const ctx =  canvas.getContext("2d");
let circleArray = [];
const colors = ['#EE2E31','#FED766',"#009FB7"];
const newColors = ['#EE4E26','#FED347',"#127FB5"]
let x,y,dx,dy,radius;
const canvasDimension = ()=>{
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
}
let mousePosition = {
    x:undefined,
    y:undefined
}
let maxRadius = 50;
window.addEventListener('mousemove',(event)=>{
    mousePosition.x= event.x;
    mousePosition.y = event.y;
})
window.addEventListener('resize',()=>{
    init()
})
console.log(mousePosition)
function Circle(x,y,dx,dy,radius,color){
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;
    this.color = color;
    this.oldColor = color;
    this.newColor = newColors[Math.floor(Math.random()*3)]
    this.minRadius = radius;
    this.draw = ()=>{
        ctx.beginPath();
        ctx.arc(this.x,this.y,this.radius,0,Math.PI * 2,false);
        ctx.strokeStyle = '#333333';
        ctx.stroke();
        ctx.fillStyle  = this.color;
        ctx.fill();
    }

    this.update = ()=>{
        if(this.x + this.radius > window.innerWidth || this.x - this.radius <=0) this.dx = -this.dx;
        if(this.y + this.radius > window.innerHeight || this.y - this.radius <=0) this.dy = -this.dy;
        this.x += this.dx;
        this.y += this.dy;
        if(mousePosition.x - this.x < 50 && mousePosition.x - this.x > -50  && mousePosition.y - this.y < 50 && mousePosition.y - this.y > -50){
            if(this.radius < maxRadius){
                this.radius +=1.8;
                this.color = this.newColor;
            }
        }else if(this.radius > this.minRadius){
            this.radius -=1;
            this.color = this.oldColor;
        }
        this.draw()
       
    }
}

const createCircles = ()=>{
    circleArray = [];
    for(let i = 0; i<1500; i++){
        radius= (Math.random()*7)+2;
        x= Math.random() * (window.innerWidth - radius*2) + radius
        y= Math.random() * (window.innerHeight- radius*2) + radius;
        dx=Math.random()*2+-1;
        dy=dx;
        circleArray.push(new Circle(x,y,dx,dy,radius,colors[Math.floor(Math.random()*3)]))
    }
    animate();
}

const animate = ()=>{
    ctx.clearRect(0,0,window.innerWidth, window.innerHeight);
    circleArray.map(circle => {circle.update()})
    requestAnimationFrame(animate);
}
const init = ()=>{
    canvasDimension();
    createCircles();
}
(()=>{
    init();
})()
